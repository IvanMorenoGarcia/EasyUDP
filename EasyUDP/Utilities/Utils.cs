﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace EasyUDP.Utilities
{
    public static class Utils
    {
        public static int Port => 1025;
        public static IPAddress LocalIP
        {
            get
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());

                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily != AddressFamily.InterNetwork) continue;

                    return IPAddress.Parse(ip.ToString());
                }

                return IPAddress.Loopback;
            }
        }

        public static string GetString(byte[] buffer, int messageLength)
        {
            return Encoding.ASCII.GetString(buffer, 0, messageLength);
        }

        public static byte[] GetBytes(string message)
        {
            return Encoding.ASCII.GetBytes(message);
        }
    }
}